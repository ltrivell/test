#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /kernel/i2c-smbus/sanity
#   Description: Confirm i2c modules load
#   Author: Laura Trivelloni <ltrivell@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
	#rlPhaseStartSetup
	#rlPhaseEnd

	rlPhaseStartTest
	rlRun "lsmod | grep i2c" 0 "No i2c module loaded."
	rlRun "modprobe i2c_dev" 0 "Loading i2c_dev."
	rlRun "modprobe i2c_algo_bit" 0 "Loading i2c_algo_bit."
	rlRun "modprobe i2c_designware" 0 "Loading i2c_designware."
	rlRun "lsmod | grep i2c" 0 "No i2c module loaded."
	rlPhaseEnd

	#rlPhaseStartCleanup
	#rlPhaseEnd
rlJournalEnd
